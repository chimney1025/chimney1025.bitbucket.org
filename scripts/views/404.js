
define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');
    var template = require('text!../../templates/404.hbs');

    // View
    var NotFoundView = Backbone.View.extend({
    	el: $("#page"),
        initialize: function () {
            Handlebars.registerPartial('divider', '<hr class="divider">');
        },

        events: {
            'click a.back': 'goBack'
        },

        goBack: function(e) {
            e.preventDefault();
            window.history.back();
        },

        render: function () {
            var tmpl = Handlebars.compile(template);
            this.$el.html(tmpl({}));

            return this;
        }
    });

    return NotFoundView;
});
