
define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');
    var template = require('text!../../templates/home.hbs');
    var sliderTemplate = require('text!../../templates/includes/home-slider.hbs');
    var productTemplate = require('text!../../templates/includes/home-product.hbs');
    var ProductListView = require('./includes/product-list-view');
    require('bootstrapCarousel');

    // Owl carousels
    require('owl.carousel');

    // View
    var HomeView = Backbone.View.extend({
    	el: $("#page"),
        initialize: function () {
            Handlebars.registerPartial('slider', sliderTemplate);
            Handlebars.registerPartial('divider', '<hr class="divider">');
        },

        render: function () {

            var tmpl = Handlebars.compile(template);
            this.$el.html(tmpl({}));

            var products = new ProductListView({
            	el: "#new-products",
                template: productTemplate
            });

            var featuredProducts = new ProductListView({
                el: "#featured-products",
                template: productTemplate
            });
            
            // Carousels
            $("#home-slider .owl-carousel").owlCarousel({
                items: 1,
                margin: 0,
                loop: true,
                dots: true,
                nav: false,
                navText: ['<span class="glyphicon  glyphicon-chevron-left"></span>','<span class="glyphicon  glyphicon-chevron-right"></span>'],
            });
            return this;
        }
    });

    return HomeView;
});
