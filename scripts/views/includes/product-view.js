define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');
    var cartItemList = require('../../collections/cart-item-list');

    // View
    var ProductView = Backbone.View.extend({
        initialize: function (options) {
            cartItemList.fetch();

            this.template = options.template;
        },

        render: function () {
            if (this.model.changed.id !== undefined) {
                return;
            }

            var result = this.model.toJSON();
            var tmpl = Handlebars.compile(this.template);
            this.$el.html(tmpl(result));

            this.$el.attr('data-price', result.price);
            this.$el.attr('data-rating', result.rating);

            return this;
        },
        
        events: {
            'click a.add-to-cart': 'addToCart'
        },


        addToCart: function (e) {
            e.preventDefault();

            var old = $(e.currentTarget).html();
            
            //check existence
            var that = this;

            var item = cartItemList.find(function(item) {
                return item.get("id") === that.model.get("id");
            });

            if (item) {
                item.save({
                    quantity: parseInt(item.get("quantity")) + 1
                }, {
                    wait: true,
                    success: function (item, response) {
                        $(e.currentTarget).html('<span class="glyphicon glyphicon-ok"></span>');

                        setTimeout(function() {
                            $(e.currentTarget).html(old);
                        }, 1000);
                    },
                    error: function (item, response) {
                    }
                });
            } else {
                cartItemList.create({
                    id: this.model.get("id"),
                    name: this.model.get("name"),
                    price: this.model.get("price"),
                    image: this.model.get("image")
                }, {
                    wait: true,
                    success: function (item, response) {
                        $(e.currentTarget).html('<span class="glyphicon glyphicon-ok"></span>');

                        setTimeout(function() {
                            $(e.currentTarget).html(old);
                        }, 1000);
                    },
                    error: function (item, response) {
                    }
                });
            }
        },
    });

    return ProductView;
});