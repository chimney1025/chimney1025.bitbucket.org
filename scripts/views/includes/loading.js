
define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');
    var template = require('text!../../templates/loading.hbs');

    // View
    var LoadingView = Backbone.View.extend({
        el: $("#page"),
        initialize: function () {
            console.log('loading');
            this.render();
        },

        render: function () {
            //this.$el.html(template);
            Handlebars.registerPartial('divider', '<hr class="divider">');
            var tmpl = Handlebars.compile(template);
            this.$el.html(tmpl({}));

            var header = new HeaderView({});

            var breadcrumbs = new BreadcrumbsView({});

            return this;
        }
    });

    return LoadingView;
});
