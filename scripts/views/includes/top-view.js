define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');

    var template = require('text!../../templates/includes/top.hbs');
    var searchTemplate = require('text!../../templates/modals/search.hbs');
    var bookingTemplate = require('text!../../templates/modals/booking.hbs');
    require('bootstrapModal');
    var timeslots = require('../../collections/delivery-time-slots');

    var selectedTime = require('models/selected-time');

    // View
    var TopView = Backbone.View.extend({
        el: "#top",

        initialize: function () {

            Handlebars.registerPartial('search-modal', searchTemplate);
            Handlebars.registerPartial('booking-modal', bookingTemplate);

            var that = this;

            /*
            timeslots.fetch({
                success: function() {
                    selectedTime.fetch({
                        success: function() {
                            that.getSlots();
                        }
                    })
                }
            });
            */
            
            selectedTime.fetch();

            timeslots.fetch({
                success: function() {
                    that.updateSelection();
                }
            });
        },

        events: {
            //'click a.search-submit': 'searchSubmit',
            'click a.delivery-date': 'setDate'
        },

        updateSelection: function() {
            var that = this;
            timeslots.each(function(slot){
                if(slot.get("id") == selectedTime.get("slot")) {
                    slot.save({
                        selected: true
                    });
                } else {
                    slot.save({
                        selected: false
                    });
                }
            }, this);
        },

        setDate: function(e) {
            var that = this;
            e.preventDefault();

            timeslots.each(function(slot){
                if(slot.get("id") == $(e.currentTarget).data("id")) {
                    slot.save({
                        id: slot.get("id"),
                        selected: true
                    });

                    selectedTime.save({
                        id: 1,
                        slot: slot.get("id"),
                        day: slot.get("day"),
                        date: slot.get("date"),
                        time: slot.get("time")
                    });

                } else {
                    slot.save({
                        id: slot.get("id"),
                        selected: false
                    });
                }
            }, this);

            if($(e.currentTarget).data("id") == 0) {
                selectedTime.save({
                    id: 1,
                    slot: "",
                    day: "",
                    date: "",
                    time: "",
                });
            }

            this.render();
        },

        updateDate: function() {
            if(selectedTime.get("slot")) {
                this.$el.find('.selected-delivery').parent().addClass("warning-color");

            } else {
                this.$el.find('.selected-delivery').parent().removeClass("warning-color");
            }
        },

        searchSubmit: function(e) {
            console.log($("#search-input").val());
        },

        render: function () {
            var tmpl = Handlebars.compile(template);
            this.$el.html(tmpl({
                timeslots: timeslots.toJSON(),
                selectedDate: selectedTime.toJSON()
            }));
            this.updateDate();
        }
    });

    return TopView;
});