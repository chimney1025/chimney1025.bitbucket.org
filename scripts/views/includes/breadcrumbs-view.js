define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');
    var template = require('text!../../templates/includes/breadcrumbs.hbs');

    // View
    var BreadcrumbsView = Backbone.View.extend({
        el: "#breadcrumbs",
        initialize: function () {
        },

        render: function (options) {
            var tmpl = Handlebars.compile(template);

            if(options.breadcrumb) {
                this.breadcrumb = options.breadcrumb;

                if(options.breadcrumb.additionalClass) {
                    this.$el.attr('class', 'breadcrumbs ' + options.breadcrumb.additionalClass);
                } else {
                    this.$el.attr('class', 'breadcrumbs');
                }

                this.$el.html(tmpl(this.breadcrumb));
            } else {
                // remove breadcrumbs class
                this.$el.removeClass("breadcrumbs");
                this.$el.html('');
            }

            return this;
        }
    });

    return BreadcrumbsView;
});