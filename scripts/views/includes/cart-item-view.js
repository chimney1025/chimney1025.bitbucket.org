define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');
    var template = require('text!../../templates/includes/cart-item.hbs');

    var CartItemView = Backbone.View.extend({
        tagName: "div",
        className: "header-cart__product  clearfix  js--cart-remove-target",
        events: {
            'click a.js--remove-item': 'removeItem'
        },
        initialize: function () {
            // replace with removeItem function 
            //this.listenTo(this.model, 'destroy', this.remove);
        },

        removeItem: function (e) {
            var that = this;

            e.preventDefault();
            var model = this.model;

            var target = e.currentTarget;
            $(target).parents($(target).data('target')).animate({
                opacity: 0
            }, 'swing', function() {
                $(this).slideUp("", function() {
                    model.destroy();
                    that.remove();
                });
                
            });
        },

        render: function (json) {
            if (this.model.changed.id !== undefined) {
                return;
            }

            var result;

            if(json) {
                result = json;
            } else {
                result = this.model.toJSON();
            }
            var tmpl = Handlebars.compile(template);
            this.$el.html(tmpl(result));

            return this;
        }
    });

    return CartItemView;
});