define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');
    var template = require('text!../../templates/includes/cart-item-checkout.hbs');

    var CartItemCheckoutView = Backbone.View.extend({
        tagName: "tr",
        className: "checkout_table_item",
        events: {
        },
        initialize: function () {
        },
        render: function () {
            if (this.model.changed.id !== undefined) {
                return;
            }

            var result = this.model.toJSON();
            var tmpl = Handlebars.compile(template);
            this.$el.html(tmpl(result));

            return this;
        }
    });

    return CartItemCheckoutView;
});