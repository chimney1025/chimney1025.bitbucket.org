define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');
    var cartItemList = require('../../collections/cart-item-list');
    var CartItemView = require('./cart-item-view');

    var CartModalView = Backbone.View.extend({
        el: ".header-cart",

        initialize: function () {
            var that = this;

            cartItemList.fetch();

            this.listenTo(cartItemList, 'reset', this.render);
            this.listenTo(cartItemList, 'change:quantity', this.render);
            this.listenTo(cartItemList, 'add', this.render);
            this.listenTo(cartItemList, 'remove', this.updateCount);
        },

        events: {
        },

        updateCount: function () {
            var sum = 0;
            var count = 0;
            cartItemList.each(function (item) {
                sum += parseFloat(item.get("price")) * item.get("quantity");
                count += parseInt(item.get("quantity"));
            }, this);

            $(".cart-total").text(parseFloat(sum).toFixed(2));
            //$(".cart-count").text(cartItemList.length);
            $(".cart-count").text(count);
        },

        render: function () {
            $(".header-cart-list").html('');
            cartItemList.each(this.renderItem, this);
        },

        renderItem: function (item) {
            var cartItemView = new CartItemView({
                model: item
            });
            $(".header-cart-list").append(cartItemView.render().el);
            this.updateCount();
        }

    });

    return CartModalView;
})