define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');
    var template = require('text!../../templates/includes/footer.hbs');

    // View
    var FooterView = Backbone.View.extend({
        el: "#footer",

        initialize: function () {
        },

        render: function () {
            var tmpl = Handlebars.compile(template);
            this.$el.html(tmpl({}));
        }
    });

    return FooterView;
});