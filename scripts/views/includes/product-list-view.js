define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var _ = require('underscore');
    var ProductList = require('collections/product-list');
    var ProductView = require('./product-view');

    // View
    var ProductListView = Backbone.View.extend({
        el: "",

        initialize: function (options) {
            var that = this;
            this.el = options.el;
            this.collection = new ProductList([]);
            this.collection.fetch();

            this.listenTo(this.collection, 'sort', this.render);
            this.listenTo(this.collection, 'reset', this.render);

            this.options = options;
        },

        render: function () {
            //this.$el.find(".productContainer").remove();
            this.$el.html('');
            this.collection.each(this.renderProduct, this);
        },

        renderProduct: function (item) {
            var productView = new ProductView({
                model: item,
                template: this.options.template
            });

            this.$el.append(productView.render().el);
            //this.updateCount();
        }

        /*
        events: {
            'change #view': 'changeView',
            'change #sort': 'sortProduct',
            'change #filter': 'changeFilter'
        },

        changeView: function (e) {
            var style = e.currentTarget.value;
            switch (style) {
            case "list":
                this.$el.find("#product-list").addClass("list-view");
                break;

            case "grid":
                this.$el.find("#product-list").removeClass("list-view");
                break;
            }
        },

        sortProduct: function (e) {
            var field = e.currentTarget.value;

            switch (field) {
            case "name":
            case "name-reverse":
                this.collection.comparator = function (model) {
                    if (model.get("name")) {
                        var str = model.get("name");
                        var reverse;
                        field == "name-reverse" ? reverse = true : reverse = false;
                        return app.sortString(str, reverse);
                    };
                }
                break;

            case "price-low":
                this.collection.comparator = function (model) {
                    return model.get("price");
                }
                break;

            case "price-high":
                this.collection.comparator = function (model) {
                    return -model.get("price");
                }
                break;

            case "popularity":
                this.collection.comparator = function (model) {
                    //return model.get(field);
                }
                break;
            }

            // call the sort method

            this.collection.sort();
        },

        //@todo improve this dropdown thing
        setFilter: function () {
            var filter = this.$el.find("#filter");
            $('#filter option', this).not(':eq("all)').remove();

            var that = this;

            this.categories.each(function (type) {
                $(filter).append(
                    $("<option></option>")
                    .attr("value", type.get("id"))
                    .text(type.get("name"))
                );
            }, this);
        },

        changeFilter: function (e) {
            var typeId = e.currentTarget.value;
            var that = this;

            if (typeId == "all") {
                this.render();
            } else {
                this.$el.find("article").remove();
                this.collection.each(function (item) {
                    var tags = item.get("tags").map(function (tag) {
                        return tag.id;
                    });
                    if (tags.indexOf(parseInt(typeId)) >= 0) {
                        that.renderProduct(item);
                    }
                }, this);
            }
        },

        updateCount: function () {
            app.countTotal.text(this.collection.length);
        },
        */
    });

    return ProductListView;
});