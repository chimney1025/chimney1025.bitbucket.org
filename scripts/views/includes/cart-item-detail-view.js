define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');
    var template = require('text!../../templates/includes/cart-item-detail.hbs');

    var CartItemDetailView = Backbone.View.extend({
        tagName: "tr",
        className: "cart_table_item",
        events: {
            'click input.js--minus-one': 'minusOne',
            'click input.js--plus-one': 'plusOne',
            'blur input.quantity__input': 'updateCount',
            'click .js--remove-item': 'removeItem'
        },

        initialize: function () {
            this.listenTo(this.model, 'change:quantity', this.render);
        },

        updateCount: function(e) {
            console.log(e.currentTarget.value);
            
            e.preventDefault();
            var $input = $(e.currentTarget);
            var number = parseInt($input.val(), 10);

            if (isNaN(number)) {
                number = 1;
            }

            if(number > 20) {
                number = 20;
            }

            if(number <= 0) {
                this.removeItem(e);
            } else {
                this.model.save({
                    quantity: number
                });
            }
        },

        minusOne: function(e) {
            e.preventDefault();
            var quantity = parseInt(this.model.toJSON().quantity);

            if(quantity == 1) {
                this.removeItem(e);
            } else {
                this.model.save({
                    quantity: quantity - 1
                });
            }
        },

        plusOne: function(e) {
            e.preventDefault();
            var quantity = parseInt(this.model.toJSON().quantity);

            if(quantity >= 20) {
                return false;
            }

            this.model.save({
                quantity: quantity + 1
            });
        },

        removeItem: function (e) {
            e.preventDefault();
            var target = e.currentTarget;

            var model = this.model;

            $(target).parents($(target).data('target')).animate({
                opacity: 0
            }, 'swing', function() {
                $(this).slideUp("", function() {
                    model.destroy();
                });
                
            });
        },

        render: function () {
            if (this.model.changed.id !== undefined) {
                return;
            }

            var result = this.model.toJSON();
            var tmpl = Handlebars.compile(template);
            this.$el.html(tmpl(result));

            return this;
        }
    });

    return CartItemDetailView;
});