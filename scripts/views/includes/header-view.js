define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');
    var template = require('text!../../templates/includes/header.hbs');
    var cartTemplate = require('text!../../templates/includes/header-cart.hbs');
    
    require('bootstrapCollapse');
    require('bootstrapModal');
    var Modernizr = require('modernizr');

    // View
    var HeaderView = Backbone.View.extend({
        el: "#header",

        initialize: function () {
            Handlebars.registerPartial('header-cart', cartTemplate);
        },

        events: {
            'click a.open-cart': 'openCart'
        },

        openCart: function(e) {
            /*
            if(!Modernizr.touch) {
                e.preventDefault();

                $('.header-cart').toggleClass("active");
            }
            */
        },

        render: function () {

            var tmpl = Handlebars.compile(template);
            this.$el.html(tmpl({}));
        }
    });

    return HeaderView;
});