define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');
    var template = require('text!../../templates/loading.hbs');

    // View
    var LoadingView = Backbone.View.extend({
        tagName: "div",
        className: "container",
        initialize: function () {},

        render: function () {
            this.$el.html(template);

            return this;
        }
    });

    return LoadingView;
});