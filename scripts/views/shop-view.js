
define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');
    var template = require('text!../../templates/shop.hbs');
    var bannerTemplate = require('text!../../templates/includes/banner.hbs');
    var productTemplate = require('text!../../templates/includes/product.hbs');
    var ProductListView = require('./includes/product-list-view');

    // View
    var ShopView = Backbone.View.extend({
    	el: $("#page"),
        initialize: function () {
            Handlebars.registerPartial('banner', bannerTemplate);
            Handlebars.registerPartial('divider', '<hr class="divider">');
            this.model = {
                productCategories: [
                    {
                        id: 1,
                        name: "休闲食品"
                    },
                    {
                        id: 2,
                        name: "烹饪食材"
                    },
                    {
                        id: 3,
                        name: "方便食品"
                    },
                    {
                        id: 4,
                        name: "冷冻食品"
                    },
                    {
                        id: 5,
                        name: "新鲜果蔬"
                    },
                    {
                        id: 6,
                        name: "生活杂物"
                    },
                    {
                        id: 7,
                        name: "其它"
                    },
                ]
            };
        },

        render: function () {
            var tmpl = Handlebars.compile(template);
            this.$el.html(tmpl(this.model));

            // content
            var products = new ProductListView({
            	el: "#products",
                template: productTemplate
            });

            return this;
        }
    });

    return ShopView;
});
