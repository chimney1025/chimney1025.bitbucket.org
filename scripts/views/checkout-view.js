
define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');
    var template = require('text!../../templates/checkout.hbs');
    var addressesTemplate = require('text!../../templates/modals/address.hbs');
    var cartItemList = require('../collections/cart-item-list');
    var orderList = require('../collections/order-list');
    var CartItemView = require('./includes/cart-item-checkout-view');

    var selectedTime = require('models/selected-time');
    var Postcode = require('models/postcode');
    var utils = require('utils');

    // View
    var CheckoutView = Backbone.View.extend({
    	el: $("#page"),
        details: {},

        initialize: function () {
            cartItemList.fetch();
            selectedTime.fetch();

            Handlebars.registerPartial('divider', '<hr class="divider">');
            this.listenTo(selectedTime, 'change', this.setTime);
        },

        events: {
            'change #checkout-form input': 'validate',
            'click .select-address': 'selectAddress',
            'click #place-order': 'placeOrder'
        },

        setTime: function() {
            if(selectedTime.get("slot")) {
                this.$el.find(".selected-time").text(selectedTime.get("day") + " " + selectedTime.get("date") + " " + selectedTime.get("time"));
            } else {
                this.$el.find(".selected-time").text("未选择");
            }
        },

        clearCache: function() {
            selectedTime.save({
                id: 1,
                slot: "",
                day: "",
                date: "",
                time: "",
            });

            var model;

            while (model = cartItemList.first()) {
              model.destroy();
            }
        },

        validate: function(e) {
            var that = this;
            var name = e.target.name;
            e.target.value = e.target.value.toUpperCase();
            var value = e.target.value;

            var required = ["postcode", "address1", "town", "fullname", "email", "phone"];
            var valid = null;

            if(required.indexOf(name) >= 0) {
                if(!value.trim()) {
                    valid = false;
                } else {
                    switch(name) {
                        case "postcode":
                            valid = utils.validPostcode(e.target.value);

                            if(valid) {
                                var postcode = new Postcode({
                                    id: value.replace(/\s/g, '')
                                });

                                this.$el.find("#address-list").html('查找地址中...');
                                $('#addressModal').modal('show');

                                postcode.fetch({
                                    data: {
                                        "api-key" : "y35aMX_sfUCd9Wpor2sATA3923"
                                    },
                                    success: function(postcode, response) {
                                        that.collection = response.Addresses;
                                        that.$el.find("#address-list").html('');

                                        if(response && response.Addresses) {
                                            that.renderAddress(response.Addresses);
                                        }
                                    },
                                    error: function(postcode, response) {
                                        if(response.status == 404) {
                                            that.$el.find("#address-list").html('没有找到结果<br>请确认邮编 '+value+' 正确');
                                        }
                                    }
                                });
                            }

                            break;

                        case "email":
                            valid = utils.validEmail(value);
                            break;

                        case "phone":
                            e.target.value = value.replace(/\s/g, '');
                            valid = utils.validPhone(e.target.value);
                            break;

                        default:
                            valid = true;
                        break;
                    }
                }
            } 

            if(valid) {
                e.target.classList.remove("error");
                e.target.classList.add("valid");
                this.details[name] = value.trim();
            } else if(valid == false) {
                e.target.classList.add("error");
                e.target.classList.remove("valid");
            }
        },

        selectAddress: function(e) {
            e.preventDefault();
            var id = $(e.currentTarget).data("id");

            if(id >= 0) {
                var address = this.collection[id];

                if(address) {
                    this.fillAddress(address);
                }
            }

            $("#address-wrap").show();
        },

        fillAddress: function(address) {
            var arr = address.split(',');
            var value = "";
            arr.map(function(line) {
                if(line.trim()) {
                    value += line.trim() + "\n";
                }
            })
            $("textarea#address").val(value);
            this.details.address = value.replace(/(?:\r\n|\r|\n)/g, '<br />');;
        },

        placeOrder: function() {
            var that = this;
            var details = this.details;

            details.day = selectedTime.get("day");
            details.date = selectedTime.get("date");
            details.time = selectedTime.get("time");
            details.notes = $('#checkout-form textarea[name="notes"]').val().replace(/(?:\r\n|\r|\n)/g, '<br />');;

            orderList.create({
                created: new Date(),
                status: "处理中",
                items: cartItemList,
                deliveryDetails: details,
                total: $($('.cart-total')[0]).text()
            }, {
                wait: true,
                success: function(order, response) {
                    that.clearCache();
                    window.location.hash = 'order-received/' + order.get("id");
                }
            });
        },

        renderAddress: function(arr) {
            var that = this;
            arr.map(function(address, index) {
                that.$el.find('#address-list').append("\
                    <div class='col-xs-12 push-down-10'>\
                      <a href='#' data-id=" + index + " \
                      data-target='addressModal' data-dismiss='modal' \
                      class='select-address btn btn-primary--reverse-transition'> \
                      " + address + 
                      "</a></div>");
            });
        },

        render: function () {
            //this.$el.html(template);
            var tmpl = Handlebars.compile(template);
            this.$el.html(tmpl({}));

            tmpl = Handlebars.compile(addressesTemplate);
            this.$el.append(tmpl({}));

            this.$el.find('.checkout_table_item').remove();
            cartItemList.each(this.renderItem, this);

            this.setTime();

            return this;
        },

        renderItem: function (item) {
            var cartItemView = new CartItemView({
                model: item
            });

            $("#cart-checkout tbody").append(cartItemView.render().el);
        }
    });

    return CheckoutView;
});
