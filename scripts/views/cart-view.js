
define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');
    var template = require('text!../../templates/cart.hbs');
    var cartItemList = require('../collections/cart-item-list');
    var CartItemView = require('./includes/cart-item-detail-view');

    var selectedTime = require('models/selected-time');

    // View
    var CartView = Backbone.View.extend({
    	el: $("#page"),
        initialize: function () {
            cartItemList.fetch();
            selectedTime.fetch();

            Handlebars.registerPartial('divider', '<hr class="divider">');
            this.listenTo(cartItemList, 'remove', this.toggleButton);
            this.listenTo(selectedTime, 'change', this.setTime);
        },

        events: {
            'click button.clear': 'removeAll',
            'click #checkout': 'checkout',
        },

        toggleButton: function() {
            if(cartItemList.length == 0) {
                $("#checkout").prop("disabled", true);
            } else {
                $("#checkout").prop("disabled", false);
            }
        },

        setTime: function() {
            if(selectedTime.get("slot")) {
                this.$el.find(".selected-time").text(selectedTime.get("day") + " " + selectedTime.get("date") + " " + selectedTime.get("time"));
            } else {
                this.$el.find(".selected-time").text("未选择");
            }
        },

        checkout: function(e) {
            e.preventDefault();

            if(!selectedTime.get("slot")) {
                $('#bookingModal').modal('show');
            } else {
                window.location = "/#checkout";
            }
        },

        render: function () {
            var tmpl = Handlebars.compile(template);
            this.$el.html(tmpl());

            this.$el.find(".cart_table_item").remove();
            cartItemList.each(this.renderItem, this);

            this.toggleButton();
            this.setTime();

            return this;
        },

        renderItem: function (item) {
            var cartItemView = new CartItemView({
                model: item
            });
            
            $("#cart-detail tbody").prepend(cartItemView.render().el);
        }
    });

    return CartView;
});
