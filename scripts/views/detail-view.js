
define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');

    require('bootstrapTab');

    // Owl carousels
    require('owl.carousel');
    require('owl.carousel2.thumbs');

    var template = require('text!../../templates/detail.hbs');
    var tabsTemplate = require('text!../../templates/includes/tabs.hbs');
    var productTemplate = require('text!../../templates/includes/product.hbs');
    var ProductListView = require('./includes/product-list-view');
    var cartItemList = require('../collections/cart-item-list');

    // View
    var DetailView = Backbone.View.extend({
    	el: $("#page"),
        maxQuantity: 20,
        initialize: function () {
            cartItemList.fetch();

            Handlebars.registerPartial('tabs', tabsTemplate);
            Handlebars.registerPartial('divider', '<hr class="divider">');
        },

        events: {
            'click a#add-to-cart': 'addToCart',
            'click .quantity .js--minus-one': 'minusOne',
            'click .quantity .js--plus-one': 'plusOne',
            'blur .quantity .quantity__input': 'checkQuantity'
        },

        checkQuantity: function(e) {
            var quantity = parseInt(e.currentTarget.value);
            var new_quantity = 1;

            if(!isNaN(quantity)) {
                if(quantity > 20) {
                    new_quantity = 20;
                } else if(quantity < 1) {
                    new_quantity = 1;
                } else {
                    new_quantity = quantity;
                }
            }

            this.$el.find("#product-detail input.quantity__input").val(new_quantity);
        },

        minusOne: function(e) {
            e.preventDefault();

            var quantity = parseInt(this.$el.find("#product-detail input.quantity__input").val());
            if(quantity > 1) {
                this.$el.find("#product-detail input.quantity__input").val(quantity - 1);
            }
            
        },

        plusOne: function(e) {
            e.preventDefault();

            var quantity = parseInt(this.$el.find("#product-detail input.quantity__input").val());
            if(quantity < this.maxQuantity) {
                this.$el.find("#product-detail input.quantity__input").val(quantity + 1);
            }

        },

        addToCart: function (e) {
            e.preventDefault();

            var old = $(e.currentTarget).html();
            $(e.currentTarget).html('添加中...');
            var quantity = parseInt(this.$el.find("#product-detail input.quantity__input").val());
            
            //check existence
            var that = this;

            var item = cartItemList.find(function(item) {
                return item.get("id") === that.model.get("id");
            });

            if (item) {
                item.save({
                    quantity: parseInt(item.get("quantity")) + quantity
                }, {
                    wait: true,
                    success: function (item, response) {
                        $(e.currentTarget).html('<span class="glyphicon glyphicon-ok"></span> 已添加');

                        setTimeout(function() {
                            $(e.currentTarget).html(old);
                        }, 1000);
                    },
                    error: function (item, response) {
                    }
                });
            } else {
                cartItemList.create({
                    id: this.model.get("id"),
                    name: this.model.get("name"),
                    price: this.model.get("price"),
                    image: this.model.get("image"),
                    quantity: quantity
                }, {
                    wait: true,
                    success: function (item, response) {
                        $(e.currentTarget).html('<span class="glyphicon glyphicon-ok"></span> 已添加!');
                        
                        setTimeout(function() {
                            $(e.currentTarget).html(old);
                        }, 1000);
                    },
                    error: function (item, response) {
                    }
                });
            }
        },

        render: function (options) {
            if(!options || !options.model) {
                return false;
            }
            
            this.model = options.model
            var tmpl = Handlebars.compile(template);
            var result = this.model.toJSON();
            this.$el.html(tmpl(result));
            
            // Carousels
            $("#detail-slider .owl-carousel").owlCarousel({
                items: 1,
                margin: 0,
                loop: true,
                dots: true,
                nav: false,
                thumbs: true
                //navText: ['<a href="btn btn-primary">','<span class="glyphicon  glyphicon-chevron-right"></span>'],
            });
            
            // related products
            var products = new ProductListView({
                el: "#related-products",
                template: productTemplate
            });

            return this;
        }
    });

    return DetailView;
});
