
define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');
    var template = require('text!../../templates/admin/admin-dashboard.hbs');
    var AdminOrdersView = require('./admin-orders');
    var AdminProductsView = require('./admin-products');
    var AdminUsersView = require('./admin-users');
    var utils = require('utils');

    // View
    var AdminView = Backbone.View.extend({
        el: $("#page"),

        initialize: function () {
            Handlebars.registerPartial('divider', '<hr class="divider">');
        },

        events: {
        },

        showError: function(error) {
            $("#error").text(error);
        },

        render: function (sub) {
            var tmpl = Handlebars.compile(template);
            this.$el.html(tmpl({}));

            switch(sub) {
                case "orders":
                var adminOrdersView = new AdminOrdersView({});
                this.$el.append(adminOrdersView.render().el);
                break;

                case "products":
                var adminProductsView = new AdminProductsView({});
                this.$el.append(adminProductsView.render().el);
                break;

                case "users":
                var adminUsersView = new AdminUsersView({});
                this.$el.append(adminUsersView.render().el);
                break;
            }

            return this;
        }
    });

    return AdminView;
});
