
define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');
    var template = require('text!../../templates/admin/admin-products.hbs');
    var ProductList = require('../../collections/product-list');
    var productList = new ProductList([]);
    var utils = require('utils');

    // View
    var AdminProductsView = Backbone.View.extend({
        initialize: function () {
            Handlebars.registerPartial('divider', '<hr class="divider">');
        },

        events: {
            'click #add-product': 'addProduct',
            'click .btn-delete': 'deleteProduct'
        },

        addProduct: function(e) {
            var that = this;
            e.preventDefault();

            var newname = $("#page #new-name").val().trim();
            var newprice = parseInt($("#page #new-price").val());
            var data;

            if(this.validateProduct("name", newname)) {
                if(this.validateProduct("price", newprice)) {
                    this.showError('');
                    data = {
                        "name": newname,
                        "price": newprice
                    };
                    productList.create(data, {
                        wait: true,
                        success: function(product, response) {
                            that.showError('Success');
                            //that.render('products');
                            that.$el.html(tmpl({
                                products: productList.toJSON().concat([product])
                            }));
                        }
                    });
                } else {
                    this.showError("Invalid product price");
                }
            } else {
                this.showError("Invalid product name");
            }
        },

        deleteProduct: function(e) {
            e.preventDefault();
            var id = $(e.currentTarget).data("id");
            productList.remove(id, {
                success: function(d, r) {
                    console.log(d);
                    console.log(r);
                },
                error: function(r) {
                    console.log(r);
                }
            })
        },

        showError: function(error) {
            $("#error").text(error);
        },

        validateProduct: function(type, value) {
            console.log(type);
            console.log(value);
            switch(type) {
                case "name":
                if(!value) {
                    return false;
                }
                break;

                case "price":
                if(!value) {
                    return false;
                }
                break;

                default:
                return true;
            }

            return true;
        },

        render: function () {
            var that = this;
            var tmpl = Handlebars.compile(template);
            productList.fetch({
                success: function(list) {
                    that.$el.html(tmpl({
                        products: list.toJSON()
                    }));
                }
            });

            return this;
        }
    });

    return AdminProductsView;
});
