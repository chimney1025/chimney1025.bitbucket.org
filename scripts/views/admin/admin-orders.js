
define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');
    var template = require('text!../../templates/admin/admin-orders.hbs');
    var orderTemplate = require('text!../../templates/modals/order.hbs');
    var buttonTemplate = require('text!../../templates/includes/update-buttons-admin.hbs');
    var orderList = require('../../collections/order-list');
    var utils = require('utils');
    var order;

    // View
    var AdminOrdersView = Backbone.View.extend({
        initialize: function () {
            Handlebars.registerPartial('divider', '<hr class="divider">');
            Handlebars.registerPartial('order-modal', orderTemplate);
            Handlebars.registerPartial('update-buttons', buttonTemplate);
        },

        events: {
            'click .show-order': 'showOrder',
            'click .update-order': 'updateOrder'
        },

        showOrder: function(e) {
            var id = $(e.currentTarget).data("id");
            order = orderList.get(id);
            this.render(order.toJSON());
        },

        updateOrder: function(e) {
            var that = this;
            var statusId = $(e.currentTarget).data("status");
            var status = "";

            switch(statusId) {
                case 1:
                status = "准备中";
                break;

                case 2:
                status = "已发货";
                break;

                case 3:
                status = "已签收";
                break;

                case 4:
                status = "订单取消";
                break;
            }

            order.save({
                status: status
            }, {
                success: function(item, response) {
                    that.render(item);
                }
            });
        },

        showError: function(error) {
            $("#error").text(error);
        },

        render: function (order) {
            var that = this;
            var tmpl = Handlebars.compile(template);
            orderList.fetch({
                success: function(list) {
                    that.$el.html(tmpl({
                        orders: list.toJSON(),
                        order: order,
                    }));
                }
            });

            return this;
        }
    });

    return AdminOrdersView;
});
