
define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');
    var template = require('text!../../templates/admin/admin-users.hbs');
    var utils = require('utils');

    // View
    var AdminUsersView = Backbone.View.extend({
        initialize: function () {
            Handlebars.registerPartial('divider', '<hr class="divider">');
        },

        events: {
        },

        showError: function(error) {
            $("#error").text(error);
        },

        render: function () {
            var that = this;
            var tmpl = Handlebars.compile(template);
            that.$el.html(tmpl({}));

            return this;
        }
    });

    return AdminUsersView;
});
