
define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');
    var template = require('text!../../templates/order-received.hbs');
    var Order = require('../models/order');

    // View
    var CheckoutView = Backbone.View.extend({
    	el: $("#page"),
        initialize: function () {
            Handlebars.registerPartial('divider', '<hr class="divider">');
        },

        render: function (id) {
            var that = this;
            var order = new Order({
                id: id
            });

            order.fetch({
                success: function(order, response) {
                    //this.$el.html(template);
                    var tmpl = Handlebars.compile(template);
                    that.$el.html(tmpl({
                        order: order.toJSON()
                    }));
                }
            })

            return this;
        }
    });

    return CheckoutView;
});
