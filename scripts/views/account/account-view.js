
define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');
    var template = require('text!../../templates/account/account-dashboard.hbs');
    var LoginView = require('./login-view');
    var AccountOrdersView = require('./account-orders');
    var utils = require('utils');
    var auth = require('auth');

    // View
    var AccountView = Backbone.View.extend({
        el: $("#page"),

        initialize: function () {
            Handlebars.registerPartial('divider', '<hr class="divider">');
            this.listenTo(auth.getAuth(), 'change', this.render);
        },

        events: {
            'click #logout': 'logout'
        },

        logout: function(e) {
            e.preventDefault();
            if(auth.logout()) {
                this.render();
            }
        },

        render: function (sub) {
            var tmpl = Handlebars.compile(template);

            if(!auth.getAuth()) {
                console.log('no auth, login');
                this.$el.html(tmpl({}));
                var loginView = new LoginView({});
                this.$el.append(loginView.render().el);
            } else {
                switch(sub) {
                    case "orders":
                        this.$el.html(tmpl({
                            username: auth.getAuth()
                        }));
                        var accountOrdersView = new AccountOrdersView({});
                        this.$el.append(accountOrdersView.render().el);
                        break;

                    default:
                        this.$el.html(tmpl({
                            username: auth.getAuth()
                        }));
                }
            }
            return this;
        }
    });

    return AccountView;
});
