
define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var _ = require('underscore');
    var template = require('text!../../templates/account/account-login.hbs');
    var utils = require('utils');
    var auth = require('auth');

    // View
    var LoginView = Backbone.View.extend({
        el: $("#page"),

        initialize: function () {
            Handlebars.registerPartial('divider', '<hr class="divider">');
        },

        events: {
            'click #login': 'login'
        },

        login: function(e) {
            e.preventDefault();
            var data = {
                username: $('#username').val(),
                password: $('#password').val()
            };
            if(utils.validLogin(data)) {
                $('#login-error').text('');
                auth.login(data, 
                function(data) {
                    console.log('success');
                    Backbone.history.loadUrl();
                },
                function(error) {

                });
            } else {
                $('#login-error').text('Invalid username/password');
            }  
        },

        showError: function(error) {
            $("#error").text(error);
        },

        render: function () {
            var tmpl = Handlebars.compile(template);
            this.$el.html(tmpl({}));

            return this;
        }
    });

    return LoginView;
});
