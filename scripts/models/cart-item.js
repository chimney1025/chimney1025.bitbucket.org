define(function(require) {
    var Backbone = require('backbone');
    var LocalStorage = require('localStorage');
    
    var CartItem = Backbone.Model.extend({
        defaults: {
            id: "",
            name: "",
            price: "",
            quantity: 1
        },
        //localStorage: new LocalStorage('cart'),
    });
    
    return CartItem;
});