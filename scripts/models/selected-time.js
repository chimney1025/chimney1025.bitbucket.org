define(function(require) {
    var Backbone = require('backbone');
    var LocalStorage = require('localStorage');
    
    var SelectedTime = Backbone.Model.extend({
        defaults: {
            id: 1,
            slot: "",
        },
        localStorage: new LocalStorage('selected-time'),
        initialize: function() {
        }
    });
    
    return new SelectedTime({});
});