define(function (require) {
    'use strict';

    var Backbone = require('backbone');
    var localStorage = require('localStorage');

    var User = Backbone.Model.extend({
        defaults: {
            id: "",
            username: "",
            password: "",
            account: "arm-findland"
        },
        urlRoot: "https://adef5625f0d2011e6baf2064b8f6627e-201443840.eu-west-1.elb.amazonaws.com:9085/v1/",
        //localStorage: new localStorage('user')
    });

    return User;
});