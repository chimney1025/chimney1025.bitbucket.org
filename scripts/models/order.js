define(function (require) {
    'use strict';

    var Backbone = require('backbone');
    var localStorage = require('localStorage');

    var Order = Backbone.Model.extend({
        defaults: {
            created: "",
            status: "",
            total: "",
            userId: "",
            deliveryDetails: {
                time: "",
                fullname: "",
                email: "",
                phone: "",
                address: "",
                postcode: "",
                notes: ""
            }
        },
        //urlRoot: "http://private-4c8f0-bestplus.apiary-mock.com/products",
        localStorage: new localStorage('order')
    });

    return Order;
});