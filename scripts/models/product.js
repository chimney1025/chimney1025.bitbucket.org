define(function (require) {
    'use strict';

    var Backbone = require('backbone');
    var localStorage = require('localStorage');

    var Product = Backbone.Model.extend({
        defaults: {
            image: "images/dummy/w263/1.jpg",
            rating: 5,
            category: "休闲食品"
        },
        urlRoot: "http://private-4c8f0-bestplus.apiary-mock.com/products"
    });

    return Product;
});