define(function(require) {
    var Backbone = require('backbone');
    var LocalStorage = require('localStorage');
    
    var DeliveryTime = Backbone.Model.extend({
        defaults: {
            id: "",
            day: "",
            date: "",
            time: "9:00 AM - 18:00 PM"
        },
        //localStorage: new LocalStorage('delivery-time'),
        initialize: function() {
        }
    });
    
    return DeliveryTime;
});