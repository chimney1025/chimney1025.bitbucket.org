define(function (require) {
    'use strict';

    var Backbone = require('backbone');

    var Postcode = Backbone.Model.extend({
        urlRoot: "https://api.getAddress.io/v2/uk/"
    });

    return Postcode;
});