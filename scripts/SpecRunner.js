require.config({
  paths: {
        jquery: '../bower_components/jquery/dist/jquery.min',
        bootstrapAffix: '../bower_components/sass-bootstrap/js/affix',
        bootstrapAlert: '../bower_components/sass-bootstrap/js/alert',
        bootstrapButton: '../bower_components/sass-bootstrap/js/button',
        bootstrapCarousel: '../bower_components/sass-bootstrap/js/carousel',
        bootstrapCollapse: '../bower_components/sass-bootstrap/js/collapse',
        bootstrapDropdown: '../bower_components/sass-bootstrap/js/dropdown',
        bootstrapModal: '../bower_components/sass-bootstrap/js/modal',
        bootstrapPopover: '../bower_components/sass-bootstrap/js/popover',
        bootstrapScrollspy: '../bower_components/sass-bootstrap/js/scrollspy',
        bootstrapTab: '../bower_components/sass-bootstrap/js/tab',
        bootstrapTooltip: '../bower_components/sass-bootstrap/js/tooltip',
        bootstrapTransition: '../bower_components/sass-bootstrap/js/transition',
        async: '../bower_components/requirejs-plugins/src/async',
        enquire: '../bower_components/enquire/dist/enquire',
        underscore: '../bower_components/underscore-amd/underscore',
        isotope: '../bower_components/isotope/jquery.isotope',
        jqueryui: 'libs/jqueryui',
        backbone: 'libs/backbone',
        handlebars: 'libs/handlebars',
        'owl.carousel': 'libs/owl.carousel.min',
        'owl.carousel2.thumbs': '../bower_components/owl.carousel2.thumbs/dist/owl.carousel2.thumbs.min',
        //'handlebars.runtime': 'handlebars',
        'handlebars.runtime': 'libs/handlebars.runtime-v4.0.5',
        localStorage: 'libs/backbone.localStorage',
        modernizr: 'libs/modernizr',
        utils: 'utils',
        templates: '../templates',
        mocha: '../node_modules/mocha/mocha',
        chai: '../node_modules/chai/chai',
        'chai-jquery': '../node_modules/chai-jquery/chai-jquery'
  },
  shim: {
    'chai-jquery': ['jquery', 'chai'],
    'mocha': {
      init: function() {
        this.mocha.setup('bdd');
        return this.mocha;
      }
    },
    'backbone': {
      deps: ['underscore', 'jquery'],
      exports: 'Backbone'
    },
    'underscore': {
      exports: '_'
    }
  },
  urlArgs: 'bust=' + (new Date()).getTime()
});

define(function(require) {
  var chai = require('chai');
  var mocha = require('mocha');
  var chaiJquery = require('chai-jquery');

  // Chai
  var should = chai.should();
  chai.use(chaiJquery);

  require([
    '/scripts/tests/models.test.js',
    '/scripts/tests/views.test.js',
  ], function(require) {
    mocha.run();
  });

});