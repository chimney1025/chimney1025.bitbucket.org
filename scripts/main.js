require.config({
    paths: {
        jquery: '../bower_components/jquery/dist/jquery.min',
        bootstrapAffix: '../bower_components/sass-bootstrap/js/affix',
        bootstrapAlert: '../bower_components/sass-bootstrap/js/alert',
        bootstrapButton: '../bower_components/sass-bootstrap/js/button',
        bootstrapCarousel: '../bower_components/sass-bootstrap/js/carousel',
        bootstrapCollapse: '../bower_components/sass-bootstrap/js/collapse',
        bootstrapDropdown: '../bower_components/sass-bootstrap/js/dropdown',
        bootstrapModal: '../bower_components/sass-bootstrap/js/modal',
        bootstrapPopover: '../bower_components/sass-bootstrap/js/popover',
        bootstrapScrollspy: '../bower_components/sass-bootstrap/js/scrollspy',
        bootstrapTab: '../bower_components/sass-bootstrap/js/tab',
        bootstrapTooltip: '../bower_components/sass-bootstrap/js/tooltip',
        bootstrapTransition: '../bower_components/sass-bootstrap/js/transition',
        async: '../bower_components/requirejs-plugins/src/async',
        enquire: '../bower_components/enquire/dist/enquire',
        underscore: '../bower_components/underscore-amd/underscore',
        isotope: '../bower_components/isotope/jquery.isotope',
        jqueryui: 'libs/jqueryui',
        backbone: 'libs/backbone',
        handlebars: 'libs/handlebars',
        'owl.carousel': 'libs/owl.carousel.min',
        'owl.carousel2.thumbs': '../bower_components/owl.carousel2.thumbs/dist/owl.carousel2.thumbs.min',
        //'handlebars.runtime': 'handlebars',
        'handlebars.runtime': 'libs/handlebars.runtime-v4.0.5',
        localStorage: 'libs/backbone.localStorage',
        modernizr: 'libs/modernizr',
        utils: 'utils',
        auth: 'auth',
        templates: '../templates'
    },
    shim: {
        bootstrapAffix: {
            deps: [
				'jquery'
			]
        },
        bootstrapAlert: {
            deps: [
				'jquery'
			]
        },
        bootstrapButton: {
            deps: [
				'jquery'
			]
        },
        bootstrapCarousel: {
            deps: [
				'jquery'
			]
        },
        bootstrapCollapse: {
            deps: [
				'jquery',
				'bootstrapTransition'
			]
        },
        bootstrapDropdown: {
            deps: [
				'jquery'
			]
        },
        bootstrapPopover: {
            deps: [
				'jquery',
                'bootstrapTooltip'
			]
        },
        bootstrapScrollspy: {
            deps: [
				'jquery'
			]
        },
        bootstrapTab: {
            deps: [
				'jquery'
			]
        },
        bootstrapTooltip: {
            deps: [
				'jquery'
			]
        },
        bootstrapModal: {
            deps: [
				'jquery'
			]
        },
        bootstrapTransition: {
            deps: [
				'jquery'
			]
        },
        'owl.carousel2.thumbs': {
            deps: [
                'owl.carousel'
            ]
        },
        'chai-jquery': {
            deps: [
                'jquery', 'chai'
            ]
        }
    }
});

require([
  // Load our app module and pass it to our definition function
  'app',

], function(App){
  // The "app" dependency is passed in as "App"
  // Again, the other dependencies passed in are not "AMD" therefore don't pass a parameter to this function
  App.initialize();
});