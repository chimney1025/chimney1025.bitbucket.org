
define(function (require) {
    var $ = require('jquery');
    var _ = require('underscore');

    /* Authentication */
    var getAuth = function() {
        return localStorage.getItem("auth");
    };

    var login = function(data, success, error) {
        localStorage.setItem('auth', data.username);
        success(data);
    };

    var logout = function() {
        localStorage.removeItem('auth');
        return true;
    }

    return {
        login: login,
        logout: logout,
        getAuth: getAuth
    }
});