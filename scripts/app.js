// Filename: app.js
define(function(require) {
    var $ = require('jquery');
    var _ = require('underscore');
    var Backbone = require('backbone');
    var Handlebars = require('handlebars');
    var AffixMenu = require('libs/AffixMenu');
    //var IsotopeShop = require('libs/IsotopeShop');
    var SimpleMap = require('libs/SimpleMap');
    var FastClick = require('libs/fastclick');

    require('libs/modernizr.min');
    require('bootstrapAffix');
    require('bootstrapButton');
    require('bootstrapCarousel');
    require('bootstrapDropdown');
    require('bootstrapPopover');
    require('bootstrapScrollspy');
    require('bootstrapTooltip');
    require('bootstrapTransition');
    require('utils');

    var Router = require('./router');

    var initialize = function() {

        // Pass in our Router module and call it's initialize function
        Router.initialize();

        (function() {
            new FastClick(document.body);
        })();
        

        // Google fonts
        (function() {
            WebFontConfig = {
                google: {
                    families: ['Arvo:700:latin', 'Open+Sans:400,600,700:latin']
                }
            };
            (function() {
                var wf = document.createElement('script');
                wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
                    '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
                wf.type = 'text/javascript';
                wf.async = 'true';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(wf, s);
            })();
        })();

        /**
         * Affix menu
         */
        (function() {
            if ($('.js--affix-menu').length > 0) {
                var sidebarMenu = new AffixMenu({
                    menuElm: '.js--affix-menu',
                    footerElm: '.js--page-footer'
                });

                enquire.register('screen and (min-width: 768px)', {
                    match: function() {
                        sidebarMenu.init();
                    },
                    unmatch: function() {
                        sidebarMenu.destroy();
                    }
                });
            }
        })();

        /**
         * Isotope Shop
        (function() {
            var shop = new IsotopeShop({
                priceSlider: $('.js--jqueryui-price-filter'),
                priceRange: [0, 20],
                priceStep: 0.2
            });
        })();

        (function() {
            if ($('.js--where-we-are').length < 1) {
                return;
            }
            var map = new SimpleMap($('.js--where-we-are'), {
                latLng: $('.js--where-we-are').data('latlng'),
                markers: $('.js--where-we-are').data('markers'),
                // markersImg: 'images/favicon.png',
                zoom: $('.js--where-we-are').data('zoom'),
                styles: [{
                    featureType: "landscape",
                    stylers: [{
                        saturation: -100
                    }, {
                        lightness: 65
                    }, {
                        visibility: "on"
                    }]
                }, {
                    featureType: "poi",
                    stylers: [{
                        saturation: -100
                    }, {
                        lightness: 51
                    }, {
                        visibility: "simplified"
                    }]
                }, {
                    featureType: "road.highway",
                    stylers: [{
                        saturation: -100
                    }, {
                        visibility: "simplified"
                    }]
                }, {
                    featureType: "road.arterial",
                    stylers: [{
                        saturation: -100
                    }, {
                        lightness: 30
                    }, {
                        visibility: "on"
                    }]
                }, {
                    featureType: "road.local",
                    stylers: [{
                        saturation: -100
                    }, {
                        lightness: 40
                    }, {
                        visibility: "on"
                    }]
                }, {
                    featureType: "transit",
                    stylers: [{
                        saturation: -100
                    }, {
                        visibility: "simplified"
                    }]
                }, {
                    featureType: "administrative.province",
                    stylers: [{
                        visibility: "off"
                    }]
                }, {
                    featureType: "administrative.locality",
                    stylers: [{
                        visibility: "off"
                    }]
                }, {
                    featureType: "administrative.neighborhood",
                    stylers: [{
                        visibility: "on"
                    }]
                }, {
                    featureType: "water",
                    elementType: "labels",
                    stylers: [{
                        visibility: "on"
                    }, {
                        lightness: -25
                    }, {
                        saturation: -100
                    }]
                }, {
                    featureType: "water",
                    elementType: "geometry",
                    stylers: [{
                        hue: "#ffff00"
                    }, {
                        lightness: -25
                    }, {
                        saturation: -97
                    }]
                }]
            }).renderMap();
        })();

         */
    };
    return {
        initialize: initialize
    };
});