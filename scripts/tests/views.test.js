define(function(require) {
    var assert = require('chai').assert;
    var ProductListView, cartItemList;
    var DetailView = require('/scripts/views/detail-view.js');

  describe('Views', function() {

    describe('Product detail view', function() {
      var detailView = new DetailView({});

      it('should return false if no options provided', function() {
        var detail = detailView.render({});
        detail.should.equal(null);
      });
    });
  });
});