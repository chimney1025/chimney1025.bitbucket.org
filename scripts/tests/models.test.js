define(function(require) {
    var assert = require('chai').assert;
    var Product = require('/scripts/models/product.js');
    var CartItem = require('/scripts/models/cart-item.js');
    var DeliveryTime = require('/scripts/models/delivery-time.js');
    var selectedTime = require('/scripts/models/selected-time.js');
    var Postcode = require('/scripts/models/postcode.js');
    var Order = require('/scripts/models/order.js');

  describe('Models', function() {

    describe('Product', function() {
      it('should default product rating to 5', function() {
        var sample = new Product({});
        sample.defaults.rating.should.equal(5);
      });
    });

    describe('Cart Item', function() {
      it('should have default quantity 1', function() {
        var sample = new CartItem({});
        sample.defaults.quantity.should.equal(1);
      });
    });

    describe('Delivery Time', function() {
      it('should have empty default day', function() {
        var sample = new DeliveryTime({});
        sample.defaults.day.should.equal("");
      });
    });

    describe('Selected Time', function() {
      it('should have empty default slot', function() {
        selectedTime.defaults.slot.should.equal("");
      });
    });

    describe('Postcode', function() {
      it('should have default url root', function() {
        var postcode = new Postcode({});
        postcode.urlRoot.should.equal("https://api.getAddress.io/v2/uk/");
      });
    });

    describe('Order', function() {
      it('should have a deliveryDetails object', function() {
        var order = new Order({});
        order.defaults.deliveryDetails.should.be.an('object');
      });
    });
  });
});