// Filename: router.js
define(function(require) {
  var $ = require('jquery');
  var _ = require('underscore');
  var Backbone = require('backbone');
  var auth = require('./auth');

  // partials views
  var TopView = require('views/includes/top-view');
  var FooterView = require('views/includes/footer-view');
  var HeaderView = require('views/includes/header-view');
  var BreadcrumbsView = require('views/includes/breadcrumbs-view');
  var NotFoundView = require('views/404');
  var CartModalView = require('views/includes/cart-modal-view');

  var selectedTime = require('models/selected-time');

  var top = new TopView({});
  var footer = new FooterView({});
  var header = new HeaderView({});
  var breadcrumbs = new BreadcrumbsView({});
  var notFound = new NotFoundView({});
  var cart = new CartModalView({});

  // pages views
  var HomeView = require('views/home-view');
  var AccountView = require('views/account/account-view');
  var ShopView = require('views/shop-view');
  var DetailView = require('views/detail-view');
  var CartView = require('views/cart-view');
  var CheckoutView = require('views/checkout-view');
  var OrderReceivedView = require('views/order-received-view');
  var LoadingView = require('views/loading');
  var AdminView = require('views/admin/admin-view');

  var homeView = new HomeView({});
  var accountView = new AccountView({});
  var shopView = new ShopView({});
  var detailView = new DetailView({});
  var cartView = new CartView({});
  var checkoutView = new CheckoutView({});
  var orderView = new OrderReceivedView({});
  var adminView = new AdminView({});

  // product model
  var Product = require('models/product');

  // router
  var AppRouter = Backbone.Router.extend({
    routes: {
      // Define some URL routes
      'shop': 'shop',
      'shop/:id': 'detail',
      'cart': 'cart',
      'checkout': 'checkout',
      'order-received/:id': 'order-received',
      '404': 'not-found',
      'account': 'account',
      'account/:sub': 'account',
      'admin': 'admin',
      'admin/:sub': 'admin',
      
      // Default
      '*actions': 'defaultAction'
    }
  });

  var defaultRender = function(bc) {
    if(!bc) {
      bc = {};
    }

      top.render();
      header.render();
      cart.render();
      breadcrumbs.render(bc);
      footer.render();
      $('#page').addClass('padding-header');
      $(document).scrollTop(0);
  };

  var adminRender = function(bc) {
      $('#top').html('');
      $('#header').html('');
      $('#page').html('');
      $('#footer').html('');
      $('#page').removeClass('padding-header');
      $(document).scrollTop(0);
  }

  var notFoundRender = function() {
      top.render();
      header.render();
      breadcrumbs.render({});
      notFound.render();
      footer.render();
      cart.render();
      $(document).scrollTop(0);
  };
  
  var initialize = function(){

    var app_router = new AppRouter;

    app_router.on('route:shop', function() {
      shopView.render();
      defaultRender();
    });

    app_router.on('route:detail', function(id) {
      var product = new Product({
        id: id
      });

      product.fetch({
        success: function(item, response) {
          
          detailView.render({
            model: item
          });

          defaultRender();
        }, 
        error: function(model, xhr, options) {
          notFoundRender();
        }
      });
      
    });

    app_router.on('route:cart', function() {
      cartView.render();

      defaultRender();
    });

    app_router.on('route:checkout', function() {
      checkoutView.render();

      defaultRender();
    });

    app_router.on('route:order-received', function(id) {
      orderView.render(id);

      defaultRender();
    });

    app_router.on('route:not-found', function() {

      notFoundRender();

    });

    app_router.on('route:account', function(sub) {
      accountView.render(sub);
      defaultRender();
    });

    app_router.on('route:admin', function(sub) {
      adminRender({});
      adminView.render(sub);
    });

    app_router.on('route:defaultAction', function (actions) {
      homeView.render();

      defaultRender({});

     // We have no matching route, lets display the home page 
      //var homeView = new HomeView();
      //homeView.render();
    });

    // Unlike the above, we don't call render on this view as it will handle
    // the render call internally after it loads data. Further more we load it
    // outside of an on-route function to have it loaded no matter which page is
    // loaded initially.
    //var footerView = new FooterView();

    Backbone.history.start();
  };
  return { 
    initialize: initialize
  };
});
