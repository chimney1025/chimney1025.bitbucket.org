/**
 * Small utilities, not
 */
define(function (require) {
    var $ = require('jquery');
    var _ = require('underscore');
    var Handlebars = require('handlebars');
    var enquire = require('enquire');

    var validLogin = function(data) {
        if(!data.username) {
            return false;
        }

        if(!data.password) {
            return false;
        }

        return true;
    };

    var validPostcode = function(postcode) {
        var postcode_regex = /[A-Z]{1,2}[0-9][0-9A-Z]?\s?[0-9][A-Z]{2}/gi;
        return postcode_regex.test(postcode);
    };

    var validEmail = function(email) {
        var email_regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return email_regex.test(email);
    };

    var validPhone = function(phone) {
        var phone_regex = /^0(\d ?){10}$/;
        return phone_regex.test(phone);
    }

    var sortString = function (str, reverse) {
        str = str.toLowerCase();
        str = str.split("");
        str = _.map(str, function (letter) {
            var result = "";

            reverse ?
                result = String.fromCharCode(-(letter.charCodeAt(0))) :
                result = String.fromCharCode((letter.charCodeAt(0)));

            return result;
        });
        return str;
    };

    Handlebars.registerHelper("showTotal", function(quantity, price) {
        return parseFloat(parseInt(quantity) * parseFloat(price)).toFixed(2);
    });

    /**
     * Close the responsive menu when resizing window
     */
    (function () {
        enquire.register('screen and (min-width: 992px)', {
            match: function () {
                $('.navbar-toggle').not('.collapsed').trigger('click');
            }
        });
    })();

    /**
     * Special helpers when we have a touch events
     */
    (function () {
        if ('ontouchstart' in document.documentElement) {
            // mobile dropdowns
            $('.js--mobile-dropdown').on('click', function (ev) {
                ev.preventDefault();

                // remove all exiting visible dropdowns
                $(this)
                    .siblings('.js--mobile-dropdown')
                    .find('.show-menu')
                    .removeClass('show-menu');


                // show the current dropdown
                $(this)
                    .find('.dropdown-menu')
                    .toggleClass('show-menu');
            });
        } else {
            // add .no-touch to the <html>
            $('html').addClass('no-touch');
        }
    })();

    /**
     * Thumbnail selector for the product
     */
    (function () {
        $('.js--preview-thumbs a').click(function (ev) {
            ev.preventDefault();
            $($(this).attr('href')).attr('src', $(this).data('src'));
            $(this).parent().addClass('active').siblings('.active').removeClass('active');
        });
    })();

    //  ==========
    //  = Highlight current date =
    //  ==========
    (function () {
        var
            timeTable = $('.js--timetable'),
            date = new Date();
        if (timeTable.length > 0) {
            date = date.getDay();
            timeTable.children('[data-day="' + date + '"]').addClass('today');
        }
    })();


    /**
     * Add the gradient to the jumbotron after the page is loaded
     */
    (function () {
        $('.js--add-gradient').addClass('jumbotron--gradient');
    })();

    return {
        sortString: sortString,
        validPostcode: validPostcode,
        validEmail: validEmail,
        validPhone: validPhone,
        validLogin: validLogin
    }
});