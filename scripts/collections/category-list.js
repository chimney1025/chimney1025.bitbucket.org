define(function(require) {
    var Backbone = require('backbone');
    var Category = require('Category');
    var $ = require('jquery');
    var _ = require('underscore');
    
    var CategoryList = Backbone.Collection.extend({

        url: "http://private-4c8f0-bestplus.apiary-mock.com/categories",

        model: Category,


        parse: function (response) {
            return response;
        },

        // Overwrite the sync method to pass over the Same Origin Policy
        sync: function (method, model, options) {
            var that = this;
            var params = _.extend({
                type: 'GET',
                dataType: 'json',
                url: that.url,
                processData: false
            }, options);

            return $.ajax(params);
        },

        getAll: function () {
            return this.where({});
        }
    });
    
    return CategoryList;
});