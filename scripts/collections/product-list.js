define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var _ = require('underscore');
    var Product = require('../models/product');
    var localStorage = require('localStorage');
    var utils = require('utils');

    var ProductList = Backbone.Collection.extend({
        model: Product,
        initialize: function () {

        },

        comparator: function (model) {
            if (model.get("name")) {
                var str = model.get("name");
                var reverse;
                return utils.sortString(str);
            }
        },

        url: "http://private-4c8f0-bestplus.apiary-mock.com/products",

        parse: function (response) {
            return response;
        },

        // Overwrite the sync method to pass over the Same Origin Policy
        sync: function (method, model, options) {
            var that = this;
            var params = _.extend({
                type: 'GET',
                dataType: 'json',
                url: that.url,
                processData: false
            }, options);

            return $.ajax(params);
        }
    });

    return ProductList;

});