define(function (require) {
    'use strict';

    var $ = require('jquery');
    var Backbone = require('backbone');
    var _ = require('underscore');
    var Order = require('../models/order');
    var localStorage = require('localStorage');
    var utils = require('utils');

    var OrderList = Backbone.Collection.extend({
        model: Order,
        initialize: function () {

        },
        localStorage: new localStorage('order'),
        //url: "http://private-4c8f0-bestplus.apiary-mock.com/products",
        parse: function (response) {
            return response;
        },

        // Overwrite the sync method to pass over the Same Origin Policy
        /*
        sync: function (method, model, options) {
            var that = this;
            var params = _.extend({
                type: 'GET',
                dataType: 'json',
                url: that.url,
                processData: false
            }, options);

            return $.ajax(params);
        }
        */
    });

    return new OrderList({});

});