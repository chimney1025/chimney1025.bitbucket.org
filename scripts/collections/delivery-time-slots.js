define(function(require) {
    var Backbone = require('backbone');
    var DeliveryTime = require('models/delivery-time');
    var LocalStorage = require('localStorage');
    var utils = require('utils');
    
    var DeliveryTimes = Backbone.Collection.extend({
        model: DeliveryTime,
        initialize: function () {
        },
        localStorage: new LocalStorage('delivery-time-slots'),
    });

    var deliveryTimeSlots = new DeliveryTimes({});

    deliveryTimeSlots.create({
        id: "1",
        day: "星期四",
        date: "07/04/2016"
    });

    deliveryTimeSlots.create({
        id: "2",
        day: "星期五",
        date: "08/04/2016"
    });

    deliveryTimeSlots.create({
        id: "3",
        day: "星期六",
        date: "09/04/2016",
        time: "9:00 AM - 22:00 PM"
    });

    deliveryTimeSlots.create({
        id: "4",
        day: "星期日",
        date: "10/04/2016",
        time: "9:00 AM - 22:00 PM"
    });

    deliveryTimeSlots.create({
        id: "5",
        day: "星期一",
        date: "11/04/2016"
    });

    deliveryTimeSlots.create({
        id: "6",
        day: "星期二",
        date: "12/04/2016"
    });
    
    return deliveryTimeSlots;
});