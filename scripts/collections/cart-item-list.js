define(function(require) {
    var Backbone = require('backbone');
    var CartItem = require('models/cart-item');
    var LocalStorage = require('localStorage');
    var utils = require('utils');
    
    var CartItemList = Backbone.Collection.extend({
        model: CartItem,
        initialize: function () {
        },

        comparator: function (model) {
            if (model.get("name")) {
                var str = model.get("name");
                var reverse;
                return utils.sortString(str);
            }
        },
        localStorage: new LocalStorage('cart'),
    });
    
    return new CartItemList({});
});