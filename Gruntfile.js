module.exports = function(grunt) {

	// Matchdep - Use globule to filter npm module dependencies by name
	var matchdep = require('matchdep');

	// Filter devDependencies (with config string indicating file to be required)
	// Load the plugin that provides the task
	matchdep.filterDev('grunt-*').forEach(grunt.loadNpmTasks);

	// Project configuration
	grunt.initConfig({

		// Run predefined tasks whenever watched file patterns are added, changed or deleted
		watch: {
			options: {
				livereload: true,
				spawn:      false
			},

			// autoprefix the files
			autoprefixer: {
				files: ['stylesheets/*.css'],
				tasks: ['autoprefixer:dev'],
			}
		},

		// Compass convert .scss to .css
		// https://github.com/gruntjs/grunt-contrib-compass
		compass: {
			options: {
				sassDir:        'sass',
				javascriptsDir: 'scripts',
				outputStyle:    'nested',
				relativeAssets: true,
				importPath:     'bower_components',
				debugInfo:      false
			},
			dev: {
				options: {
					imagesDir:      'images',
					cssDir:         'stylesheets',
					environment:    'development',
					noLineComments: false,
					watch:          true
				},
				files: [{
					expand: true,
					cwd:    'sass/',
					src:    '{,*/}*.scss',
					dest:   'stylesheets',
					ext:    '.css'
				}]
			},
			build: {
				options: {
					imagesDir:      'build/images',
					cssDir:         'build/stylesheets',
					environment:    'production',
					noLineComments: true,
					watch:          false
				},
				files: [{
					expand: true,
					cwd:    'sass/',
					src:    '{,*/}*.scss',
					dest:   'build/stylesheets',
					ext:    '.css'
				}]
			}
		},

		// Parse CSS and add vendor-prefixed CSS properties using the Can I Use database. Based on Autoprefixer.
		// https://github.com/nDmitry/grunt-autoprefixer
		autoprefixer: {
			dev: {
				files: [{
					expand: true,
					cwd:    'stylesheets/',
					src:    '*.css',
					dest:   'stylesheets'
				}]
			},
			build: {
				files: [{
					expand: true,
					cwd:    'build/stylesheets/',
					src:    '*.css',
					dest:   'build/stylesheets'
				}]
			}
		},

		// Minify PNG, JPG and GIF images
		// https://github.com/gruntjs/grunt-contrib-imagemin
		imagemin: {
			options: {
				optimizationLevel: 3
			},
			build: {
				files: [{
					expand: true,
					cwd:    'images',
					src:    ['**/*.{png,gif,jpg,jpeg}'],
					dest:   'build/images'
				}]
			}
		},

		// requireJS optimizer
		// https://github.com/gruntjs/grunt-contrib-requirejs
		requirejs: {
			build: {
				// Options: https://github.com/jrburke/r.js/blob/master/build/example.build.js
				options: {
					baseUrl:                 'scripts',
					mainConfigFile:          'scripts/main.js',
					optimize:                'uglify2',
					preserveLicenseComments: false,
					useStrict:               true,
					wrap:                    true,
					name:                    'bower_components/requirejs/require',
					include:                 'main',
					out:                     'build/js/main.js'
				}
			}
		},

		// https://github.com/yeoman/grunt-usemin
		useminPrepare: {
			html: 'build/index.html',
			options: {
				dest: 'build'
			}
		},
		usemin: {
			html: ['build/*.{html,php}'],
			css:  ['build/stylesheets/{,*/}*.css']
		},

		// rename the files based on the content
		// https://github.com/cbas/grunt-rev
		rev: {
			files: {
				src: ['build/**/*.css']
			}
		},

		// concurrent
		// https://github.com/sindresorhus/grunt-concurrent
		concurrent: {
			dev: ['compass:dev', 'watch']
		},

		// copy some files not handled by other tasks
		// https://github.com/gruntjs/grunt-contrib-copy
		copy: {
			build: {
				files: [{
					expand: true,
					cwd:    'bower_components',
					src:    ['sass-bootstrap/fonts/*'],
					dest:   'build/bower_components'
				},
				{
					expand: true,
					cwd:    'assets/zocial/css',
					src:    ['*'],
					dest:   'build/assets/zocial/css'
				}]
			}
		},

		// clean the build dir
		// https://github.com/gruntjs/grunt-contrib-clean
		clean: {
			beforebuild: ['build'],
			afterBuild:  ['.tmp', 'build/stylesheets/*.bootstrap.css']
		}
	});

	// Default task(s).
	grunt.registerTask('default', [
		'autoprefixer:dev',
		'concurrent:dev'
	]);

	// building
	grunt.registerTask('build', [
		'clean:beforebuild',
		'newer:imagemin:build',
		'compass:build',
		'autoprefixer:build',
		'requirejs',
		'useminPrepare',
		'concat', // automatically configured by usemin
		'cssmin', // automatically configured by usemin
		'rev',
		'usemin',
		'copy:build',
		'clean:afterBuild'
	]);
};